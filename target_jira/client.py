from target_hotglue.client import HotglueSink
from base64 import b64encode
import json
from datetime import datetime
from target_jira.auth import OAuth2Authenticator
import requests
from singer_sdk.plugin_base import PluginBase
from typing import Any, Dict, List, Optional, Callable, cast


class JiraSink(HotglueSink):
    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        """Initialize target sink."""
        self._target = target
        self.tasks_issuetypes = None
        self.tasks_priorities = None
        self.tasks = {}
        self.statuses = None
        self.cloud_id = None
        self.projects = {}
        self.transitions = {}
        super().__init__(target, stream_name, schema, key_properties)

    @property
    def base_url(self) -> str:
        org = self.config.get("org")
        base_url = (
            f"https://api.atlassian.com/ex/jira/{self._get_cloud_id()}/rest/api/3/"
        )
        return base_url

    def _get_cloud_id(self):
        if self.cloud_id:
            return self.cloud_id
        headers = self.http_headers
        params = self.params
        response = requests.request(
            method="GET",
            url="https://api.atlassian.com/oauth/token/accessible-resources",
            params=params,
            headers=headers,
        )
        response.raise_for_status()
        cloud_id = next(
            (
                d["id"]
                for d in response.json()
                if d["name"] == self.config.get("site_name")
            ),
            None,
        )
        if not cloud_id:
            raise ("Not authorized for site_name")
        self.cloud_id = cloud_id
        return cloud_id

    @property
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        return OAuth2Authenticator(
            self._target, "https://auth.atlassian.com/oauth/token"
        )

    @property
    def http_headers(self):
        headers = {"Authorization": f"Bearer {self.authenticator.access_token}"}
        return headers

    def get_task_types(self):
        if self.tasks_issuetypes is None:
            response = self.request_api("GET", endpoint="issuetype")
            self.tasks_issuetypes = response.json()
        return self.tasks_issuetypes

    def get_account_id(self, query):
        params = {"maxResults": 1, "query": query}
        response = self.request_api("GET", endpoint="user/search", params=params)
        users = response.json()
        if users:
            return users[0]["accountId"]

        return None

    def get_priority_id(self, priority_name):
        priority_id = None
        priorities = self.get_priorities()
        for priority in priorities:
            if priority["name"].lower() == priority_name.lower():
                priority_id = priority["id"]
                break
        return priority_id

    def get_priorities(self):
        if self.tasks_priorities is None:
            response = self.request_api("GET", endpoint="priority")
            self.tasks_priorities = response.json()

        return self.tasks_priorities

    def get_project(self, project_key):
        if project_key in self.projects:
            return self.projects[project_key]
        response = self.request_api("GET", endpoint=f"project/{project_key}")
        project = response.json()
        if "id" in project:
            self.projects[project_key] = project
        return project

    def get_task_id(self, task_name, project_id):
        # TODO: Default task id?
        task_id = "10003"
        tasks = self.get_task_types()
        for task in tasks:
            if task.get("scope"):
                scope_project = task["scope"]["project"]["id"]
                if task["name"] == task_name and project_id == scope_project:
                    task_id = task["id"]
            else:
                if task["name"] == task_name:
                    task_id = task["id"]
        return task_id

    def get_project_id(self, project_key):
        project = self.get_project(project_key)
        if "id" in project:
            return project["id"]
        else:
            return False

        return task_id

    def get_task_detail(self, task_key):
        response = self.request_api("GET", endpoint=f"issue/{task_key}")
        task = response.json()
        if "id" in task:
            self.projects[task_key] = task
        return task

    def get_statuses(self):
        if self.statuses is None:
            response = self.request_api("GET", endpoint="status")
            self.statuses = response.json()
        return self.statuses

    def get_transitions(self, task_id):
        if task_id in self.transitions:
            return self.transitions[task_id]
        response = self.request_api("GET", endpoint=f"issue/{task_id}/transitions")
        self.transitions[task_id] = response.json().get("transitions")
        return self.transitions[task_id]

    def get_status_id(self, name, project_id):

        statuses = self.get_statuses()
        for status in statuses:
            if status.get("scope"):
                scope_project = status["scope"]["project"]["id"]
                if status["name"] == name and project_id == scope_project:
                    status_id = status["id"]
            else:
                if status["name"] == name:
                    status_id = status["id"]
        return status_id

    def get_transitions_id(self, name, task_id):
        records = self.get_transitions(task_id)
        return_id = None
        for row in records:
            if row["name"] == name:
                return_id = row["id"]
        return return_id

    def get_task_status(self, task_id):
        status = None
        task_details = self.get_task_detail(task_id)
        if task_details.get("fields"):
            task_details = task_details.get("fields")
            status = task_details.get("status")
            if status:
                status = status.get("name")
        return status

    def update_task_status(self, task_id, status, project_id):
        new_status_id = self.get_transitions_id(status, task_id)
        endpoint = f"issue/{task_id}/transitions"
        payload = {"transition": {"id": new_status_id}}
        response = self.request_api(
            http_method="POST", request_data=payload, endpoint=endpoint
        )
        if response.status_code == 204:
            return True
        else:
            return False

    def upsert_record(self, record: dict, context: dict):
        endpoint = "issue"
        method = "POST"
        state_dict = dict()
        id = None
        if "id" in record:
            id = record["id"]
            endpoint = f"{endpoint}/{record['id']}"
            del record["id"]
            method = "PUT"

        response = self.request_api(
            http_method=method, request_data=record, endpoint=endpoint
        )
        if response.status_code in [200, 201]:
            state_dict["success"] = True
            id = response.json().get("key")
        elif response.status_code == 204 and method == "PUT":
            state_dict["is_updated"] = True
        return id, response.ok, state_dict
