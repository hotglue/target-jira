"""Jira target sink class, which handles writing streams."""

from target_jira.client import JiraSink


class TicketsSink(JiraSink):
    name = "Tickets"
    endpoint = "issue"

    def process_issues(self, record):
        if record.get("project_key"):
            project_id = self.get_project_id(record.get("project_key"))

        mapping = {
            "fields": {
                "project": {"id": project_id},
                "labels": record.get("tags"),
                "duedate": record.get("due_date"),
                "summary": record.get("title"),
                "issuetype": {
                    "id": self.get_task_id(record.get("type", "Task"), project_id)
                },
            }
        }

        if record.get("description"):
            mapping["fields"]["description"] = {
                "content": [
                    {
                        "content": [
                            {"text": record.get("description"), "type": "text"}
                        ],
                        "type": "paragraph",
                    }
                ],
                "type": "doc",
                "version": 1,
            }

        if record.get("assignee_email"):
            mapping["fields"]["assignee"] = {
                "id": self.get_account_id(record["assignee_email"])
            }
        elif record.get("assignee_name"):
            mapping["fields"]["assignee"] = {
                "id": self.get_account_id(record["assignee_name"])
            }

        if record.get("reporter_name"):
            reporter = {"id": self.get_account_id(record["reporter_name"])}
            mapping["reporter"] = reporter
            mapping["fields"]["reporter"] = reporter

        if record.get("priority"):
            mapping["fields"]["priority"] = {
                "id": self.get_priority_id(record["priority"])
            }

        if record.get("id"):
            mapping["id"] = record["id"]
            task_status = self.get_task_status(record["id"])
            if record.get("status"):
                if task_status != record.get("status"):
                    # update status here
                    self.update_task_status(record["id"], record["status"], project_id)

        return mapping

    def preprocess_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        payload = self.process_issues(record)
        return payload


class SubTasksSink(JiraSink):
    name = "SubTasks"
    endpoint = "issue"

    def process_subtask(self, record):
        if record.get("list_id"):
            project_id = record.get("list_id")
        elif record.get("project_key"):
            project_id = self.get_project_id(record.get("project_key"))
        mapping = {
            "fields": {
                "project": {"id": project_id},
                "parent": {"key": record.get("parent_name")},
                "summary": record.get("subject"),
                "issuetype": {
                    "id": self.get_task_id(record.get("task_type", "Task"), project_id)
                },
                "description": {
                    "content": [
                        {
                            "content": [
                                {"text": record.get("description"), "type": "text"}
                            ],
                            "type": "paragraph",
                        }
                    ],
                    "type": "doc",
                    "version": 1,
                },
            }
        }
        if record.get("assignees"):
            if len(record["assignees"]) > 0:
                mapping.update({"assignee": {"name": record["assignees"][0]["name"]}})
        return mapping

    def preprocess_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        payload = self.process_subtask(record)
        return payload
