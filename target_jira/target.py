"""Jira target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_hotglue.target import TargetHotglue
from target_jira.sinks import (
    TicketsSink,
    SubTasksSink,
)


class TargetJira(TargetHotglue):
    """Sample target for Jira."""

    def __init__(
        self,
        config=None,
        parse_env_config: bool = False,
        validate_config: bool = True,
        state: str = None,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, parse_env_config, validate_config, state)

    SINK_TYPES = [TicketsSink, SubTasksSink]
    name = "target-jira"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property("site_name", th.StringType, required=True),
    ).to_dict()


if __name__ == "__main__":
    TargetJira.cli()
